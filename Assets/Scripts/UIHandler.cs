﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIHandler : MonoBehaviour {

	bool ChangeBanner;
	public Image SplashImage;
	public GameObject MainMenuScreen;
	public GameObject Splashscreen;
	public GameObject SpecificMovieScreen1;
	public GameObject SpecificMovieScreen2;

	public GameObject FoodPage1;
	public GameObject FoodPage2;
	public GameObject TicketPage;

	//public Sprite[] MovieImagers= new Sprite[4];
	public Sprite[] SplashImagers= new Sprite[4];
	int i;

	// Use this for initialization
	void Start ()
	{
		GameManager.ScreenState = 0;
		if(GameManager.ScreenState != 3)
		{			
			//Splashscreen.GetComponent<Image>().sprite=SplashImagers[Random.Range(0,SplashImagers.Length)];
			ChangeBanner = false;
			StartCoroutine (ChangeTopImage());
			i = 0;
		}

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) 
		{
			if(GameManager.ScreenState == 0)//Splash
			{
				Application.Quit ();
			}
			else if(GameManager.ScreenState == 1)//Main Menu
			{
				Application.Quit ();
			}
			else if(GameManager.ScreenState == 2)//Spesific
			{
				BacktToMovieMenu ();
			}
			else if(GameManager.ScreenState == 3)//Camera
			{
				BackClick ();
			}
		}
		if(ChangeBanner)
		{
			ChangeBanner = false;
			SplashImage.sprite = SplashImagers [i];
			i++;
			if(i>3)
				i=0;
			StartCoroutine (ChangeTopImage());
		}
	}
	public void TryNowClick()
	{
		print ("AAAAAAAAAAAAAAAAAAA");
		Splashscreen.SetActive (false);
		MainMenuScreen.SetActive (true);
		GameManager.ScreenState = 1;
	}
	public void BackClick()
	{
		SceneManager.LoadScene (0);
		GameManager.ScreenState = 1;
	}
	public void TryARClick()
	{
		SceneManager.LoadScene (1);
		GameManager.ScreenState = 3;
	}
	public void BacktToMovieMenu()
	{
		FoodPage1.SetActive (false);
		TicketPage.SetActive (false);
		SpecificMovieScreen1.SetActive (false);
		SpecificMovieScreen2.SetActive (false);
		MainMenuScreen.SetActive (true);
		GameManager.ScreenState =1;
	}
	public void SpesificClick1()
	{
		MainMenuScreen.SetActive (false);
		SpecificMovieScreen1.SetActive (true);
		GameManager.ScreenState =2;
	}
	public void SpesificClick2()
	{
		MainMenuScreen.SetActive (false);
		SpecificMovieScreen2.SetActive (true);
		GameManager.ScreenState =2;
	}
	public void BuyFood()
	{
		SpecificMovieScreen1.SetActive (false);
		FoodPage1.SetActive (true);
	}
	public void Foodnext()
	{
		FoodPage1.SetActive (false);
		FoodPage2.SetActive (true);
	}
	public void BackTOFoodPage1()
	{
		FoodPage1.SetActive (true);
		FoodPage2.SetActive (false);
	}

	public void BuyTickets()
	{
		SpecificMovieScreen1.SetActive (false);
		TicketPage.SetActive (true);
	}

	IEnumerator ChangeTopImage()
	{
		yield return new WaitForSeconds (5f);
		ChangeBanner = true;

	}
//	public IEnumerator FadeHandler(Image topBanner,float startTime,float endTime,float lerpTime )
//	{
//		float timeStartedLerpin = Time.time;
//		float timeSinceStarted = Time.time - timeStartedLerpin;
//		float percentageComplete = timeSinceStarted / lerpTime;
//
//		while (true)
//		{
//			timeSinceStarted = Time.time - timeStartedLerpin;
//			percentageComplete = timeSinceStarted / lerpTime;
//			float currentValue = Mathf.Lerp (startTime,endTime,percentageComplete);
//			topBanner.al
//			yield return new WaitForEndOfFrame ();
//		}
//
//	}
}
